package com.example.recyclerview.viewmodels;

import com.example.recyclerview.models.Book;

import java.util.ArrayList;
import java.util.List;

public class VMBooks {
    private List<Book> books;

    public VMBooks() {

        //data to populate the RecyclerView
        List<Book> books = new ArrayList<>();
        books.add(new Book("img01","Harry Potter","J.K. Rowling"));
        books.add(new Book("img02","The Lord of the Rings","J.R.R. Tolkien"));
        this.books = books;
    }

    public List<Book> getBooks() {
        return books;
    }
}
