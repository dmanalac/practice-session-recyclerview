package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.recyclerview.demo1.BookList;
import com.example.recyclerview.demo2.ClassicBooks;

public class MainActivity extends AppCompatActivity {
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        Button btnSimpleRecyclerView = findViewById(R.id.btnRecyclerView);
        Button btnHorizontalRecyclerView = findViewById(R.id.btnHorizontal);

        btnSimpleRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BookList.class);
                startActivity(intent);
            }
        });

        btnHorizontalRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ClassicBooks.class);
                startActivity(intent);
            }
        });
    }
}
