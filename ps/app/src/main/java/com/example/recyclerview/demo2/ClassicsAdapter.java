package com.example.recyclerview.demo2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerview.R;
import com.example.recyclerview.models.Book;

import java.util.ArrayList;

public class ClassicsAdapter extends RecyclerView.Adapter<ClassicsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Book> bookItems;
    private ItemClickListener itemClickListener;

    // data is passed into the constructor
    ClassicsAdapter(Context context, ArrayList<Book> items) {
        this.context = context;
        this.bookItems = items;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ClassicsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.classic_book_items, parent, false));
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Book book = bookItems.get(position);
        int imageName = context.getResources().getIdentifier(book.getImage(), "drawable", context.getPackageName());
        holder.imgBookCover.setImageResource(imageName);
        holder.txtBookAuthor.setText(book.getAuthor());
        holder.txtBookTitles.setText(book.getTitle());
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return bookItems.size();
    }

    //stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgBookCover;
        TextView txtBookTitles;
        TextView txtBookAuthor;

        ViewHolder(View itemView) {
            super(itemView);
            imgBookCover = itemView.findViewById(R.id.imgBookCover);
            txtBookTitles = itemView.findViewById(R.id.txtBookNames);
            txtBookAuthor = itemView.findViewById(R.id.txtBookAuthor);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null) itemClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    //allows click events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}

