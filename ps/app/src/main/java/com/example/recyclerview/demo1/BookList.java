package com.example.recyclerview.demo1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.recyclerview.R;
import com.example.recyclerview.models.Book;
import com.example.recyclerview.viewmodels.VMBooks;

import java.util.ArrayList;
import java.util.List;

public class BookList extends AppCompatActivity implements BookStoreAdapter.ItemClickListener {
    private BookStoreAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);
        recyclerView = findViewById(R.id.rvBooks);

        VMBooks vmBooks = new VMBooks();

        setUpRecyclerView(vmBooks.getBooks());

    }

    public void setUpRecyclerView(List<Book> data) {
        //set up RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new BookStoreAdapter(this, data);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    public void onItemClick(View view, int position) {
        Toast.makeText(this,"You clicked " + adapter.getItem(position) + " on row number " + (position + 1), Toast.LENGTH_SHORT).show();
    }
}
