package com.example.recyclerview.demo2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.recyclerview.JSONUtils;
import com.example.recyclerview.R;
import com.example.recyclerview.models.Book;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class ClassicBooks extends AppCompatActivity implements ClassicsAdapter.ItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classic_books);
        Context context = this;

        //data to populate the RecyclerView
        ArrayList<Book> books = JSONUtils.parseObjectFromAsset(context, "books.json", "classic_books", new TypeToken<ArrayList<Book>>() {});

        //set up RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rvClassicBooks);
        ClassicsAdapter adapter = new ClassicsAdapter(context, books);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();
        pagerSnapHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "You clicked page number " + (position + 1), Toast.LENGTH_SHORT).show();
    }
}