package com.example.recyclerview;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class JSONUtils {

    public static <T> ArrayList<T> parseObjectFromAsset(Context context, String jsonFile, String jsonObject, TypeToken<ArrayList<T>> typeToken) {
        String json, jsonItems;
        Gson gson = new Gson();
        Type type = typeToken.getType();
        AssetManager assetManager = context.getAssets();
        try {
            //get from assets folder
            InputStream is = assetManager.open(jsonFile);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
            JSONObject object = new JSONObject(json);
            JSONArray jsonArray = object.getJSONArray(jsonObject);
            jsonItems = jsonArray.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return gson.fromJson(jsonItems, type);
    }
}
