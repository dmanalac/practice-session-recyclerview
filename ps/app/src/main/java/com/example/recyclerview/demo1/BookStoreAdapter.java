package com.example.recyclerview.demo1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerview.R;
import com.example.recyclerview.models.Book;

import java.util.List;

public class BookStoreAdapter extends RecyclerView.Adapter<BookStoreAdapter.ViewHolder> {

    private Context context;
    private List<Book> bookItems;
    private ItemClickListener itemClickListener;

    // data is passed into the constructor
    BookStoreAdapter(Context context, List<Book> items) {
        this.context = context;
        this.bookItems = items;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.book_store_items,parent,false));
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Book book = bookItems.get(position);
        holder.txtBookTitles.setText(book.getTitle());
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return bookItems.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtBookTitles;
        TextView txtBookAuthor;

        ViewHolder(View itemView) {
            super(itemView);
            txtBookTitles = itemView.findViewById(R.id.txtBookNames);
            txtBookAuthor = itemView.findViewById(R.id.txtBookAuthor);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null) itemClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // method for getting data at click position
    Book getItem(int id) { return bookItems.get(id); }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}