package com.example.recyclerview.models;

public class Book {
    private String image;
    private String title;
    private String author;

    public Book(String image, String title, String author) {
        this.image = image;
        this.title = title;
        this.author = author;
    }

    public String getImage() { return image; }

    public String getTitle() { return title; }

    public String getAuthor() { return author; }
}
